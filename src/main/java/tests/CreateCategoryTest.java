package tests;

import org.openqa.selenium.WebDriver;
import utils.BaseScript;
import utils.GeneralActions;

public class CreateCategoryTest extends BaseScript {

    private static String login = "webinar.test@gmail.com" ;
    private static String password = "Xcg7299bnSmMuRLp9ITw";
    private static String categoryName = "Test Create New Category";

    public static void main(String[] args) throws Exception {

        WebDriver driver =  getConfiguredDriver();

        GeneralActions actions = new GeneralActions(driver);

        actions.login(login, password); //Вход в учетную запись

        actions.createCategory(categoryName);  //Создание Новой Категории

        actions.checkCreateNewCategory();  //Проверка Создания Новой Категории
        driver.close();
        driver.quit();
    }
}


package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.SkipException;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class BaseScript {
    public static WebDriver getDriver() {
        String browser = Properties.getBrowser();
        //   String browser = "firefox";
        //   String browser = "internetExplorer";

        switch (browser) {
            case "firefox":
                String driverPathGeck = System.getProperty("user.dir") + "/src/main/resources/geckodriver.exe";
                if (driverPathGeck == null)
                    throw new SkipException("Path to Chrome is not specified!");
                System.setProperty("webdriver.chrome.driver", driverPathGeck);
                return new FirefoxDriver();
            case "internetExplorer":
                String driverPathIEdriver = System.getProperty("user.dir") + "/src/main/resources/IEDriverServer.exe";
                if (driverPathIEdriver == null)
                    throw new SkipException("Path to Chrome is not specified!");
                System.setProperty("webdriver.chrome.driver", driverPathIEdriver);
                return new InternetExplorerDriver();
            default:
                System.setProperty("webdriver.chrome.driver",
                        new File(BaseScript.class.getResource("/chromedriver.exe").getFile()).getPath());
                return new ChromeDriver();
        }
    }
    public static EventFiringWebDriver getConfiguredDriver() {
        WebDriver driver = getDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        EventFiringWebDriver wrappedDriver = new EventFiringWebDriver(driver);
        wrappedDriver.register(new EventHandler());
        return wrappedDriver;
    }
}



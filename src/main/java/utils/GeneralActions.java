package utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GeneralActions extends Properties{
    private WebDriver driver;
    private WebDriverWait wait;
    private By loginInput = By.id("email");
    private By passwInput = By.id("passwd");
    private By submBut = By.xpath("//button[@class='btn btn-primary btn-lg btn-block ladda-button']");
    private By catalogueLink = By.cssSelector("#subtab-AdminCatalog");
    private By categoriesLink = By.cssSelector("#subtab-AdminCategories");
    private By createNewCatLoc = By.id("page-header-desc-category-new_category");
    private By newCatNameInputLoc = By.cssSelector("#name_1");
    private By newMetaNameLoc = By.cssSelector("#meta_title_1");
    private By submitNewCatLoc = By.cssSelector("#category_form_submit_btn");
    private By messCreatNewCategLoc = By.xpath("//div[@class='alert alert-success']");
    private By filtrName = By.xpath("//input[@name='categoryFilter_name']");
    private By resultName = By.xpath("(//td[@class='pointer'])[1]");
    private By searchBut = By.id("submitFilterButtoncategory");

    public GeneralActions(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }
    public static void sleep(){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public void login(String login, String password) {
        driver.navigate().to(getBaseAdminUrl());
        WebElement loginInput = driver.findElement(this.loginInput);
        WebElement passwInput = driver.findElement(this.passwInput);
        WebElement submBut = driver.findElement(this.submBut);
        loginInput.clear();
        loginInput.sendKeys(login);
        passwInput.clear();
        passwInput.sendKeys(password);
        submBut.click();
        sleep();
    }
    public void createCategory(String categoryName) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(catalogueLink));
        WebElement catalogueLink = driver.findElement(this.catalogueLink);    //кнопка Каталог
        WebElement categoriesLink = driver.findElement(this.categoriesLink);  //кнопка Категории
                //создаем цепь действий с неявным элементом
        Actions actions = new Actions(driver);
        actions.moveToElement(catalogueLink).click(categoriesLink);
        Action mouseOverAndClick = actions.build();
        mouseOverAndClick.perform();

        wait.until(ExpectedConditions.visibilityOfElementLocated(createNewCatLoc));  //явное ожидание кнопки Создать новую Категорию
        WebElement createNewCatLoc = driver.findElement(this.createNewCatLoc);       //кнопка Создать Новую Категорию
        createNewCatLoc.click();                                                     //нажимаем Создать
        wait.until(ExpectedConditions.visibilityOfElementLocated(newCatNameInputLoc));
        WebElement newCatNameInputLoc = driver.findElement(this.newCatNameInputLoc);  //поле Имя
        newCatNameInputLoc.sendKeys(categoryName);          //вводим название новой категории
        WebElement newMetaNameLoc = driver.findElement(this.newMetaNameLoc);         //поле Мета Имя
        newMetaNameLoc.sendKeys(categoryName);             //вводим название новой МетаКатегории
        WebElement submitNewCatLoc = driver.findElement(this.submitNewCatLoc);       //кнопка Создать
        submitNewCatLoc.click();
                                              //Сообщение об успехе
        wait.until(ExpectedConditions.visibilityOfElementLocated(messCreatNewCategLoc));
        WebElement messCreatNewCategLoc = driver.findElement(this.messCreatNewCategLoc);
        System.out.println("Result is  --> " + messCreatNewCategLoc.getText());     //в Консоль текст кнопки об Успехе
    }
    public void checkCreateNewCategory (){        //Проверка результата создания Новой Категории
        WebElement filtrName = driver.findElement(this.filtrName);
        filtrName.sendKeys("Test Create New Category");
        WebElement searchBut = driver.findElement(this.searchBut);
        searchBut.click();
                                                    // Вывод в консоль результат проверки
        WebElement resultSearch = driver.findElement(this.resultName);
        System.out.print("Creating ");
        /////////// получаем и выводим в Консоль текст Сообщения об Успешном создании Категории //////////////////
        System.out.print(resultSearch.getText() + "  =  ");
        System.out.println(resultSearch.isDisplayed());
    }
}


